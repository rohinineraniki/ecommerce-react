import React, { createContext, useEffect, useState } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import axios from "axios";

import RenderProducts from "./components/RenderProducts";
import Header from "./components/Header";
import Loader from "./components/Loader";
import Footer from "./components/Footer";

import ShowError from "./components/ShowError";
import DisplaySingleProduct from "./components/DisplaySingleProduct";
import NotFound from "./components/NotFound";
import CartItem from "./components/CartItem";

import AddProduct from "./components/AddProduct";
import EditProduct from "./components/EditProduct";

import "./App.css";

export const CartProductDetails = createContext();

function App(props) {
  const [products, setProducts] = useState([]);

  const [apiError, setApiError] = useState(false);
  const [loading, setLoading] = useState(false);

  const [cartProducts, setCartProducts] = useState([]);
  const [productTobeEdit, setProductTobeEdit] = useState({});

  console.log("in app cartproducts", cartProducts);

  useEffect(() => {
    setLoading(true);
    axios
      .get("https://fakestoreapi.com/products")
      .then((response) => {
        console.log("product axios", response);
        setLoading(false);
        setProducts(response.data);
      })
      .catch((error) => {
        setLoading(false);
        setApiError(true);
        console.log(error);
      });
  }, []);

  const productAdded = (product) => {
    setProducts([...products, product]);
  };

  const isSelectedToEdit = (eachProduct) => {
    setProductTobeEdit(eachProduct);
  };

  const showUpdatedProduct = (eachProduct) => {
    console.log("app.showupdateproduct", eachProduct);
    const newUpdatedProducts = products.map((each) => {
      if (each.id === eachProduct.id) {
        return eachProduct;
      } else {
        return each;
      }
    });
    console.log("newUpdatedProducts ", newUpdatedProducts);
    setProducts(newUpdatedProducts);
  };

  const removeProduct = (eachProduct) => {
    const productsAfterRemove = products.filter((each) => {
      return each.id !== eachProduct.id;
    });
    setProducts(productsAfterRemove);
  };

  return (
    <BrowserRouter>
      <CartProductDetails.Provider value={{ cartProducts, setCartProducts }}>
        <Header />
        <Routes>
          <Route
            exact
            path="/"
            element={
              <>
                {loading && <Loader />}
                {apiError && <ShowError />}
                <RenderProducts
                  products={products}
                  isSelectedToEdit={isSelectedToEdit}
                  removeProduct={removeProduct}
                />
              </>
            }
          />
          <Route
            exact
            path="/products/:id"
            element={<DisplaySingleProduct products={products} />}
          />
          <Route
            exact
            path="/cart"
            element={<CartItem products={products} />}
          />
          <Route
            path="/addproduct"
            element={<AddProduct productAdded={productAdded} />}
          />
          <Route
            path="/updateproducts"
            element={
              <EditProduct
                selectedProduct={productTobeEdit}
                showUpdatedProduct={showUpdatedProduct}
              />
            }
          />
          <Route path="*" element={<NotFound />} />
        </Routes>
      </CartProductDetails.Provider>
      <Footer />
    </BrowserRouter>
  );
}

export default App;

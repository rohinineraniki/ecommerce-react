import React, { useState } from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faIndianRupeeSign } from "@fortawesome/free-solid-svg-icons";

function ShowProduct(props) {
  const { eachProduct, isSelectedToEdit, removeProduct } = props;
  const { category, description, id, image, price, rating, title } =
    eachProduct;

  const [edit, setEdit] = useState(false);
  const [displayRemovePopup, setDisplayRemovePopup] = useState(false);

  const handleEditProduct = () => {
    isSelectedToEdit(eachProduct);
    setEdit(true);
  };

  const handleRemove = () => {
    setDisplayRemovePopup(true);
  };

  const handleYesRemove = () => {
    removeProduct(eachProduct);
  };

  const handleNoRemove = () => {
    setDisplayRemovePopup(false);
  };

  return (
    <>
      <div className="product-container">
        <Link to={`/products/${id}`} className="link">
          <img src={image} alt={title} className="product-image" />

          <div className="title-container">
            <h5 className="title">{title}</h5>
            <div className="cost-container">
              <FontAwesomeIcon
                icon={faIndianRupeeSign}
                size="sm"
                className="rupee-icon"
              />
              <h3>{price}</h3>
            </div>
          </div>
        </Link>
        <Link to="/updateproducts">
          <button
            className="btn btn-success w-100 mb-3"
            onClick={handleEditProduct}
          >
            Update Product
          </button>
        </Link>
        <button className="btn btn-danger w-auto" onClick={handleRemove}>
          Delete Product
        </button>
        {displayRemovePopup && (
          <div className="display-add-popup">
            <h4>Are You sure to Delete product?</h4>
            <button
              className="btn btn-danger w-auto m-3"
              onClick={handleYesRemove}
            >
              Yes, Delete
            </button>
            <button className="btn btn-success w-auto" onClick={handleNoRemove}>
              Not Delete
            </button>
          </div>
        )}
      </div>
    </>
  );
}
export default ShowProduct;

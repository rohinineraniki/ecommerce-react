import React from "react";
import { useNavigate } from "react-router-dom";
import image from "../../src/images/empty-bag.png";

function NoCartItem() {
  const navigate = useNavigate();
  return (
    <div className="empty-cart-container">
      <img src={image} alt="empty-cart" className="cart-empty-image" />
      <button onClick={() => navigate("/")} className="go-home">
        View Products
      </button>
    </div>
  );
}

export default NoCartItem;

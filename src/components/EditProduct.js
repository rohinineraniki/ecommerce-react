import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

function EditProduct(props) {
  const navigate = useNavigate();
  const { selectedProduct, showUpdatedProduct } = props;
  const { category, description, id, image, price, rating, title } =
    selectedProduct;
  console.log("selected product to edit", selectedProduct);

  const [editProduct, setEditProduct] = useState({
    id: id,
    title: title,
    description: description,
    image: image,
    price: price,
    category: category,
    rating: rating,
  });

  const [displayUpdatePopup, setDisplayUpdatePopup] = useState(false);

  const handleEditForm = (event) => {
    event.preventDefault();
    console.log("editproduct", editProduct);
    showUpdatedProduct(editProduct);
    event.target.reset();
    setDisplayUpdatePopup(true);
  };

  const handleChange = (event) => {
    setEditProduct({ ...editProduct, [event.target.name]: event.target.value });
  };

  const handleSeeProduct = (event) => {
    navigate("/");
  };

  return (
    <div className="edit-product-container d-flex flex-column align-items-center">
      <form onSubmit={handleEditForm} className="mt-4 w-50">
        <h3 className="mt-3">Edit selected product below</h3>
        <div className="form-group">
          <label htmlFor="title">Title</label>
          <input
            type="text"
            id="title"
            onChange={handleChange}
            value={editProduct.title}
            name="title"
            className="form-control"
            placeholder="Product Title"
            required
          />
        </div>
        <div className="form-group">
          <label htmlFor="category">Category</label>
          <input
            type="text"
            id="category"
            onChange={handleChange}
            name="category"
            className="form-control"
            placeholder="Product Category"
            value={editProduct.category}
            required
          />
        </div>
        <div className="form-group">
          <label htmlFor="description">Description</label>
          <input
            type="text"
            id="description"
            name="description"
            onChange={handleChange}
            value={editProduct.description}
            className="form-control"
            placeholder="Product Description"
            required
          />
        </div>
        <div className="form-group">
          <label htmlFor="price">Price</label>
          <input
            className="form-control"
            type="number"
            onChange={handleChange}
            name="price"
            value={editProduct.price}
            placeholder="Product Price"
            required
          />
        </div>
        <div className="form-group">
          <label htmlFor="image">image</label>
          <input
            className="form-control"
            type="text"
            value={editProduct.image}
            onChange={handleChange}
            name="image"
            placeholder="Product Image URL"
            required
          />
        </div>
        <button type="submit" className="btn btn-warning w-auto m-3">
          Update Product
        </button>
        <button
          type="Home"
          className="btn btn-warning w-auto"
          onClick={() => navigate("/")}
        >
          Home
        </button>
      </form>
      {displayUpdatePopup && (
        <div className="display-add-popup">
          <h4>Product Updated Successfully</h4>
          <button className="btn btn-success w-auto" onClick={handleSeeProduct}>
            See Products
          </button>
        </div>
      )}
    </div>
  );
}

export default EditProduct;

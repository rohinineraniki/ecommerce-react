import React from "react";

function ShowError() {
  return (
    <div className="error-msg">
      <h1>There is an issue with showing products, Please try again later</h1>
    </div>
  );
}

export default ShowError;

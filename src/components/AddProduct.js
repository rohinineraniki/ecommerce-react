import React, { useState } from "react";
import { v4 as uuidv4 } from "uuid";
import { useNavigate } from "react-router-dom";
import image from "../../src/images/empty-bag.png";

function AddProduct(props) {
  const navigate = useNavigate();

  const { productAdded } = props;
  console.log("id", uuidv4());
  const [productFields, setFieldsAdded] = useState({
    id: uuidv4(),
    title: "",
    description: "",
    image: "",
    price: 0,
    rating: { rate: 0, count: 0 },
    category: "",
  });

  const [displayAddPopup, setDisplayAddPopup] = useState(false);

  const changeTitle = (event) => {
    setFieldsAdded({ ...productFields, title: event.target.value });
  };

  const changeCategory = (event) => {
    setFieldsAdded({ ...productFields, category: event.target.value });
  };

  const changeUrl = (event) => {
    setFieldsAdded({ ...productFields, image: event.target.value });
  };

  const changePrice = (event) => {
    setFieldsAdded({ ...productFields, price: event.target.value });
  };

  const changeDescription = (event) => {
    setFieldsAdded({ ...productFields, description: event.target.value });
  };

  const changeRating = (event) => {
    console.log("in change rate", { ...productFields.rating });
    setFieldsAdded({
      ...productFields,
      rating: { ...productFields.rating, rate: event.target.value },
    });
  };

  const changeRatingCount = (event) => {
    console.log("in change count", { ...productFields.rating });
    setFieldsAdded({
      ...productFields,
      rating: { ...productFields.rating, count: event.target.value },
    });
  };

  const handleAddForm = (event) => {
    event.preventDefault();

    console.log("productfields", productFields);
    productAdded(productFields);
    event.target.reset();
    setFieldsAdded({
      title: "",
      description: "",
      price: 0,
      rating: { rate: 0, count: 0 },
      category: "",
      id: "",
    });
    setDisplayAddPopup(true);
  };

  const handleSeeProduct = () => {
    setDisplayAddPopup(false);
    navigate("/");
  };

  return (
    <div className="add-product-container">
      <form onSubmit={handleAddForm} className="add-form">
        <div className="form-group">
          <label htmlFor="title">Add title</label>
          <input
            type="text"
            id="title"
            className="form-control"
            onChange={changeTitle}
            required
          ></input>
        </div>
        <div className="form-group">
          <label htmlFor="category">Category</label>
          <input
            type="text"
            id="category"
            className="form-control"
            required
            onChange={changeCategory}
          />
        </div>
        <div className="form-group">
          <label htmlFor="url">Image URL</label>
          <input
            type="text"
            id="url"
            className="form-control"
            required
            onChange={changeUrl}
          />
        </div>
        <div className="form-group">
          <label htmlFor="description">Description</label>
          <input
            type="text"
            id="description"
            className="form-control"
            onChange={changeDescription}
            required
          />
        </div>
        <div className="form-group">
          <label htmlFor="price">Price (in Ruppes)</label>
          <input
            type="number"
            id="price"
            className="form-control"
            required
            onChange={changePrice}
          />
        </div>
        <div className="form-group">
          <label htmlFor="rating">Rating out of 5</label>
          <input
            type="number"
            id="rating"
            max="5"
            className="form-control"
            required
            onChange={changeRating}
          />
        </div>
        <div className="form-group">
          <label htmlFor="rating-count">Number of votes</label>
          <input
            type="number"
            id="rating-count"
            className="form-control"
            required
            onChange={changeRatingCount}
          />
        </div>

        <button type="submit" className="btn btn-success w-auto align-center">
          Add Product
        </button>
      </form>
      {displayAddPopup && (
        <div className="display-add-popup">
          <h4>Product added Successfully</h4>
          <button className="btn btn-success w-auto" onClick={handleSeeProduct}>
            See Products
          </button>
        </div>
      )}
    </div>
  );
}

export default AddProduct;

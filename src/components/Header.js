import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCartArrowDown } from "@fortawesome/free-solid-svg-icons";
import CartItem from "./CartItem";
import { useNavigate } from "react-router-dom";

function Header() {
  const navigate = useNavigate();
  const handleCartContainer = () => {
    return navigate("/cart");
  };

  const clickAppLogo = () => {
    navigate("/");
  };

  return (
    <header>
      <div className="button-container">
        <div>
          <h1 className="heading" onClick={clickAppLogo}>
            {" "}
            eShop
          </h1>
        </div>
        <div>
          <button
            className="add-product-button w-auto h-75 p-2"
            onClick={() => {
              navigate("/addproduct");
            }}
          >
            Add New Product
          </button>
        </div>
        <div className="cart-container">
          <div className="buttons">
            <button className="para">Login</button>
            <button className="para">Signup</button>
          </div>
          <div className="cart-icon-container" onClick={handleCartContainer}>
            <FontAwesomeIcon
              icon={faCartArrowDown}
              size="2xl"
              style={{ color: "#e0e316" }}
            />
            <p className="para">Cart</p>
          </div>
        </div>
      </div>
      <ul className="small-device">
        <li>Clothes</li>
        <li>Jewellary</li>
        <li>Electonics</li>
        <li>Televisions</li>
      </ul>
    </header>
  );
}

export default Header;

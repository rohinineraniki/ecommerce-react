import React from "react";

import ShowProduct from "./ShowProduct";

function RenderProducts(props) {
  const { products, onClickProduct, isSelectedToEdit, removeProduct } = props;

  const showProducts = products.map((each) => {
    return (
      <ShowProduct
        eachProduct={each}
        onClickProduct={onClickProduct}
        key={each.id}
        isSelectedToEdit={isSelectedToEdit}
        removeProduct={removeProduct}
      />
    );
  });
  console.log("showproducts", showProducts);
  return <div className="main-container">{showProducts}</div>;
}

export default RenderProducts;

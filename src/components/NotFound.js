import React from "react";
import { useNavigate } from "react-router-dom";

function NotFound() {
  const navigate = useNavigate();
  return (
    <div className="notfound-container">
      <h1>404</h1>
      <h1>Not Found</h1>
      <p>The requested resource not found</p>
      <button onClick={() => navigate("/")} className="go-home">
        Go Home Page
      </button>
    </div>
  );
}

export default NotFound;

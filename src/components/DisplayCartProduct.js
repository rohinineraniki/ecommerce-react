import React, { useContext, useReducer, useState } from "react";
import { CartProductDetails } from "../App";

const reducer = (state, action) => {
  console.log("reducer state values", state);

  switch (action) {
    case "increment":
      return { initial: state.initial + 1 };
    case "decrement":
      if (state.initial > 1) {
        return { initial: state.initial - 1 };
      } else {
        return { initial: 1 };
      }

    default:
      return { initial: 1 };
  }
};

function DisplayCartProduct(props) {
  const { cartProducts, setCartProducts } = useContext(CartProductDetails);
  const { cartItem } = props;
  const [removeClick, setRemoveClick] = useState(false);

  const { id, title, category, image, price, rating, description } = cartItem;

  const [count, dispatch] = useReducer(reducer, { initial: 1 });

  console.log("count value", count);

  const handleRemoveClick = () => {
    setRemoveClick(true);
  };

  const handleConfirmRemove = () => {
    const remainProducts = cartProducts.filter((each) => {
      return each !== String(id);
    });
    console.log(remainProducts);
    setCartProducts(remainProducts);
  };

  const handleConfirmNoClick = () => {
    setRemoveClick(false);
  };

  return (
    <>
      <div className="cart-product-single-container">
        <div className="cart-image-container">
          <img src={image} className="cart-image" alt={title} />
          <div className="quantity-container">
            <button className="inc-btn" onClick={() => dispatch("decrement")}>
              -
            </button>
            <div className="count">quantity: {count.initial}</div>
            <button className="inc-btn" onClick={() => dispatch("increment")}>
              +
            </button>
          </div>
        </div>

        <div className="cart-product-single-title-container">
          <h6>{title}</h6>
          <p>{category}</p>
          <div className="cart-cost-container">
            <i className="fa-solid fa-indian-rupee-sign fa-xs cart-rupee-icon"></i>
            <p>{(price * count.initial).toFixed(2)}</p>
          </div>
          <button className="remove-cart-button" onClick={handleRemoveClick}>
            Remove Item
          </button>
        </div>
      </div>
      {removeClick ? (
        <div className="confirm-remove-container">
          <h5>Are you sure You want to remove this item?</h5>
          <div>
            <button
              onClick={handleConfirmRemove}
              className="remove-cart-button mx-3"
            >
              Yes, Remove
            </button>
            <button
              className="remove-cart-button"
              onClick={handleConfirmNoClick}
            >
              No
            </button>
          </div>
        </div>
      ) : (
        " "
      )}
    </>
  );
}

export default DisplayCartProduct;

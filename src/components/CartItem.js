import React, { useContext } from "react";
import { CartProductDetails } from "../App";
import NoCartItem from "./NoCartItem";
import DisplayCartProduct from "./DisplayCartProduct";

function CartItem(props) {
  const { cartProducts, setCartProducts } = useContext(CartProductDetails);
  const { products } = props;

  console.log("cart ids", cartProducts);
  const filteredProducts = cartProducts.map((eachId) => {
    const clickIdProduct = products.filter((product) => {
      return String(product.id) === eachId;
    });
    return clickIdProduct[0];
  });
  console.log("filterd", filteredProducts);

  const displayCartProduct = () => {
    const productInCart = filteredProducts.map((cartItem) => {
      return (
        <DisplayCartProduct
          cartItem={cartItem}
          cartProducts={cartProducts}
          setCartProducts={setCartProducts}
          key={cartItem.id}
        />
      );
    });
    return productInCart;
  };

  const displayPrice = () => {
    const totalPrice = filteredProducts.reduce((acc, each) => {
      console.log("each price", each.price);
      return acc + each.price;
    }, 0);
    console.log("price", totalPrice);
  };

  return (
    <div className="product-cart-container">
      {cartProducts.length === 0 ? <NoCartItem /> : displayCartProduct()}
      {cartProducts.length !== 0 && displayPrice()}
    </div>
  );
}

export default CartItem;

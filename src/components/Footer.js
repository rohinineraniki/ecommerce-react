import React from "react";

function Footer() {
  return (
    <footer>
      <h1 className="heading">eShop</h1>
      <div className="footer-container">
        <div className="contact-container">
          <p className="footer-para">Home</p>
          <p className="footer-para">Contact Us</p>
        </div>
        <div className="social-container">
          <p className="footer-para">Social media</p>
        </div>
      </div>
    </footer>
  );
}

export default Footer;

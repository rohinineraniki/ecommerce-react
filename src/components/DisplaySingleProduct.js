import React, { useContext, useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import axios from "axios";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faIndianRupeeSign } from "@fortawesome/free-solid-svg-icons";
import { faStar } from "@fortawesome/free-solid-svg-icons";

import Loader from "./Loader";
import ShowError from "./ShowError";
import CartItem from "./CartItem";
import { CartProductDetails } from "../App.js";

// export const ProductIdContext = React.createContext();

function DisplaySingleProduct(props) {
  const { id } = useParams();
  const navigate = useNavigate();
  console.log("props.products", props.products);
  const [singleProductData, setSingleProductData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [apiError, setApiError] = useState(false);
  const [status, setStatus] = useState(false);

  const { cartProducts, setCartProducts } = useContext(CartProductDetails);

  useEffect(() => {
    const filteredSingleProduct = props.products.filter((each) => {
      return String(each.id) === id;
    });
    console.log("filteredSingleProduct", filteredSingleProduct);
    setSingleProductData(filteredSingleProduct[0]);
    setStatus(true);
  }, []);

  const handleGoback = () => {
    navigate("/");
    setSingleProductData([]);
    setStatus(false);
  };

  const handleCart = () => {
    console.log("at add to cart button", cartProducts);
    if (!cartProducts.includes(String(id))) {
      console.log("not exists");
      setCartProducts([...cartProducts, id]);
    } else {
      navigate("/cart");
    }
  };
  console.log("singleProductData", singleProductData);
  const { category, description, image, price, rating, title } =
    singleProductData;
  console.log("rating", rating, category);

  return (
    <>
      {/* {loading && <Loader />}
    {apiError && <ShowError />} */}
      (
      {status && (
        <div className="single-product-container">
          <img src={image} alt={title} className="single-product-image" />
          <div className="about-product">
            <h2>{title}</h2>
            <p className="para">{description}</p>
            <div className="rating-container">
              <FontAwesomeIcon
                icon={faStar}
                size="lg"
                style={{ color: "#e0e316" }}
              />
              <p className="rating-para">{rating.rate}</p>
            </div>
            <p className="para">Total ratings {rating.count}</p>
            <p className="para"> Product Catagory {category}</p>
            <div className="cost-container">
              <FontAwesomeIcon
                icon={faIndianRupeeSign}
                size="lg"
                className="rupee-icon"
              />
              <h1>{price}</h1>
            </div>

            <button className="cart-button" onClick={handleCart}>
              {cartProducts.includes(String(id))
                ? "View in Cart"
                : "Add to cart"}
            </button>
            <button onClick={handleGoback} className="back-button">
              Back
            </button>
          </div>
        </div>
      )}
      )
    </>
  );
}

export default DisplaySingleProduct;
